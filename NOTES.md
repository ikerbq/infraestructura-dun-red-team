# Notes

## General notes

become: yes != become: true en ansible??


### Nessus configuration

#### General
node: Blossom
CT ID: 201
Hostname: Nessus
Unprivileged container: true
Nesting: true
Resource Pool: TFG-iker

#### Template

Storage: local
Template: ubuntu-22.04-standard_22.04-1_amd64.tar.zst

#### Disks

rootfs
Storage: local
Disk size (GiB): 8

#### CPU

Cores: 3

#### Memory

Memory (MiB): 2000
Swap (MiB): 512

#### Network

Name: eth0
MAC address: auto
Bridge: vmbr1
VLAN Tag: no VLAN
Firewall: true
IPv4: DHCP
IPv4/CIDR: -
Gateway (IPv4): -
IPv6: Static
IPv6/CIDR: None
Gateway (IPv6): 

#### DNS

DNS domain: use host settings
DNS servers: use host settings

## Useful Links

- ip and ss instead of ifconfig and netstat
https://vmware.github.io/photon/assets/files/html/3.0/photon_admin/use-ip-and-ss-commands.html

- 

## Ansible requirements

### Control Node

- UNIX-like machine: Red Hat, Debian, Ubuntu, macOS, BSDs and WSL
- Ansible installation
- Python installation

### Managed node

- Python installation (no ansible)
    - network modules do not require Python
- Account which to connect through SSH to a POSIX shell

## Ansible particular installation

### Instructions

python3 --verion
python --version
wget https://bootstrap.pypa.io/get-pip.py -O get-pip.py
alias python=python3
python get-pip.py
python3 -m pip install --user ansible-core


### Version

Python 3.11.8 | Installed dependencies:
- MarkupSafe-2.1.5
- PyYAML-6.0.1
- ansible-core-2.16.6
- cffi-1.16.0
- cryptography-42.0.7
- jinja2-3.1.4
- packaging-24.0
- pycparser-2.22
- resolvelib-1.0.1


## Terraform initialization response

Initializing the backend...

Initializing provider plugins...
- Finding kreuzwerker/docker versions matching "~> 3.0.1"...
- Installing kreuzwerker/docker v3.0.2...
- Installed kreuzwerker/docker v3.0.2 (self-signed, key ID BD080C4571C6104C)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.

