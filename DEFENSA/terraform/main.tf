# Configuration for the Proxmox provider and resources. Documentation in https://registry.terraform.io/providers/bpg/proxmox/latest/docs
terraform {
  required_providers {
    proxmox = {
      source = "bpg/proxmox"
      version = "0.60.1"
    }
  }
}
 
provider "proxmox" {
  endpoint    = "https://ip-proxmox:8006/"
  username    = "usuario@pam"
  password    = "contraseña"
  insecure    = true
 
  ssh {
    agent       = false
    username    = "usuario-ssh"
    password    = "contraseña-ssh"
    private_key = file("~/.ssh/clave-privada")
  }
}

resource "proxmox_virtual_environment_container" "nessus" {
  description = "Managed by Terraform"
 
  node_name = "Blossom"
  vm_id     = 201
 
  pool_id = "TFG-iker"
  started = true
 
  initialization {
    hostname = "Nombre-Contenedor"
 
    ip_config {
      ipv4 {
        address = "ip-contenedor/24"
      }
    }
 
    user_account {
      password = "contraseña-predeterminada"
    }
  }
 
  cpu {
    architecture = "amd64"
    cores = 3
  }
 
  memory {
    dedicated = "2048"  # Megabytes
    swap      = "512"    # Megabytes
  }
 
  network_interface {
    name = "eth0"
    bridge = "vmbr1"
  }
 
  operating_system {
    template_file_id = "local:vztmpl/ubuntu-22.04-standard_22.04-1_amd64.tar.zst"
    type             = "ubuntu"
  }
 
  mount_point {
    volume = "local-lvm"
    size   = "8G"
    path   = "/mnt/volume"
  }
 
}
