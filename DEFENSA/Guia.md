# Guía de uso de las automatizaciones Terraform y Ansible

Esta guía proporciona instrucciones sobre cómo utilizar las automatizaciones Terraform y Ansible para gestionar contenedores en un entorno virtual Proxmox e instalar un servicio en un contenedor de destino.

## Automatización Terraform

Terraform se utiliza para aprovisionar y gestionar el ciclo de vida de los contenedores en Proxmox.

### Prerrequisitos

- Instalar Terraform en el nodo de gestión.
- Acceso al Entorno Virtual Proxmox con los permisos necesarios.
- Par de claves SSH para comunicación segura con los contenedores.

### Configuración

1. **Configuración del Proveedor:**

   Crea un fichero llamado `provider.tf` con el siguiente contenido:

   ```hcl
   provider "proxmox" {
     endpoint    = "https://direccion-ip:8006/"
     username    = "usuario@pam"
     password    = "contraseña"
     insecure    = true
   }
   ```

   Sustituye `endpoint`, `username` y `password` por los datos de tu servidor Proxmox.

2. **Aprovisionamiento de contenedores

   Crea un archivo llamado `main.tf` con la configuración del contenedor:

   ```hcl
   resource "proxmox_virtual_environment_container" "nombre-del-contenedor" {
    description = "Managed by Terraform"
 
    node_name = "Blossom"
    vm_id     = 20X
  
    pool_id = "TFG-iker"
    started = true
  
    initialization {
      hostname = "Servicio"
  
      ip_config {
        ipv4 {
          address = "10.10.18.4X/24"
        }
      }
  
      user_account {
        password = "contraseña"
      }
    }
  
    cpu {
      architecture = "amd64"
      cores = 3
    }
  
    memory {
      dedicated = "2048"  # Megabytes
      swap      = "512"    # Megabytes
    }
  
    network_interface {
      name = "eth0"
      bridge = "vmbr1"
    }
  
    operating_system {
      template_file_id = "local:vztmpl/ubuntu-22.04-standard_22.04-1_amd64.tar.zst"
      type             = "ubuntu"
    }
  
    mount_point {
      volume = "local-lvm"
      size   = "8G"
      path   = "/mnt/volume"
    }
   }
   ```

### Uso

1. **Inicialización:**

   Inicializa el directorio de trabajo de Terraform:

   ```bash
   terraform init
   ```

2. **Planificación:**

   Crea un plan de ejecución:

   ```bash
   terraform plan -out=plan.out
   ```

3. **Aplicar cambios:**

   Aplica la configuración para crear el contenedor:

   ```bash
   terraform apply plan.out
   ```

4. **Destrucción de recursos:**

   Para eliminar la infraestroctura previamente creada, ejecuta

   ```bash
   terraform destroy
   ```

## Automatización Ansible

Ansible se utiliza para configurar software en los contenedores, en este caso, la instalación de un servicio de la infraestructura.

### Prerrequisitos

- Instalar Ansible en el nodo de gestión.
- Acceso SSH al contenedor de destino.

### Configuración

1. **Configuración del inventario:**

   Crear un archivo de inventario llamado `inventory.ini`:

   ```ini
   [nombre_grupo_servicio]
   NombreContenedor ansible_host=ip_contenedor ansible_user=root ansible_ssh_private_key_file=/path/to/private_key
   ```

2. **Creación del libro de jugadas:**

   Crea un archivo playbook llamado `install_servicio.yml`:

   ```yaml
   ---
   - name: Instalar Servicio en NombreContenedor
     hosts: nombre_grupo_servicio
     become: yes
     tasks:
       # ... (tareas para instalar el servicio como se haría en el contenedor destino)
      - name: Install required packages
        apt:
          name:
            - ca-certificates
            - curl
            - gnupg
          state: present
          update_cache: yes

      - name: Start Nessus service
        systemd:
          name: nessusd
          state: started
          enabled: yes
   ```

### Uso

1. **Ejecución del Playbook:**

   Ejecuta el playbook para instalar Servicio:

   ```bash
   ansible-playbook -i inventory.ini install_nessus.yml
   ```

2. **Verificación de la instalación:**

   Comprueba el estado del servicio en el contenedor de destino:

   ```bash
   ansible nessus_hosts -i inventory.ini -m systemd -a "name=daemon_del_servicio state=started"
   ```