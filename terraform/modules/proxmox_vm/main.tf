terraform {
  required_providers {
    proxmox = {
      source = "bpg/proxmox"
      version = "0.60.1"
    }
  }
}

provider "proxmox" {
  # Configuration for the Proxmox provider. Documentation in https://registry.terraform.io/providers/bpg/proxmox/latest/docs
  endpoint    = var.endpoint
  username    = var.username
  password    = var.password
  insecure    = var.insecure
  #ssh        = var.ssh
  #tmp_dir     = var.tmp_dir
}

resource "proxmox_lxc" "Nessus" {
  ctid        = var.nessus-ctid
  hostname    = var.nessus-hostname
  target_node = var.target-node
  # clone       = "nessus_template"
  password    = var.nessus-password
  ostemplate  = var.nessus-ostemplate
  disk        = var.nessus-disk
  cpu         = var.nessus-cpu
  memory      = var.nessus-memory
  swap        = var.nessus-swap
  network     = var.nessus-network

}
