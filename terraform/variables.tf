/* Small usage guide:
Allowed types:
  string
  number
  bool
  list(<TYPE>)
  set(<TYPE>)
  map(<TYPE>)
  object({<ATTR NAME> = <TYPE>, ...})
  tuple([<TYPE>, ...])

sensitive = true
  Setting a variable as sensitive prevents Terraform from showing its
  value in the plan or apply output, when you use that variable
  elsewhere in your configuration
*/

variable "endpoint" {
  description = "The Proxmox API endpoint URL"
  type        = string
}

variable "username" {
  description = "The Proxmox user for authentication"
  type        = string
}

variable "password" {
  description = "The Proxmox password for authentication"
  type        = string
  sensitive   = true
}

variable "insecure" {
  description = "Wether Proxmox TLS certificate is up to date or not"
  type        = bool
  sensitive   = true
}

variable "target-node" {
  description = "The Proxmox node where the VM will be created"
  type        = string
}

variable "resourcegroup" {
  description = "The Proxmox resource group the VM or Container will be included in"
  type        = string
}

variable "nessus-ctid" {
  description = "The Nessus container ID"
  type        = number
}

variable "nessus-hostname" {
  description = "The Nessus Host name"
  type        = string
}

variable "nessus-password" {
  description = "The Nessus Host password"
  type        = string
  sensitive   = true
}

variable "nessus_ostemplate" {
  description = "The name of the Proxmox VM template to clone for the new VM"
  type        = string

  validation {
    condition     = length(var.nessus_ostemplate) > 13 && substr(var.nessus_ostemplate, 0, 13) == "local:vztmpl/"
    error_message = "The nessus_ostemplate value must be a valid local image, starting with \"local:vztmpl/\"."
  }
}

variable "nessus-disk" {
  description = "The Nessus Host disk information"
  type        = map(string)
}

variable "nessus-cpu" {
  description = "The Nessus Host number of cores"
  type        = number
}

variable "nessus-memory" {
  description = "The Nessus Host memory (MiB)"
  type        = number
}

variable "nessus-swap" {
  description = "The Nessus Host swap memory (MiB)"
  type        = number
}

variable "nessus-network" {
  description = "The Nessus Host network information"
  type        = map(any)
}

/*
variable "proxmox_vm_config" {
    description = "Configuration for Proxmox VMs"
    type = map(any)
}

variable "Nessus" {
  description = "The name of the Proxmox node where the VM will be created"
  type        = string
}

variable "nessus_template" {
  description = "The name of the Proxmox VM template to clone for the new VM"
  type        = string
}


variable "aws_lambda_config" {
    description = "Configuration for AWS Lambda functions"
    type = map(any)
}
*/